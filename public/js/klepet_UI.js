/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function izlusciSlike(vhodnoBesedilo) {
  var besede = vhodnoBesedilo ? vhodnoBesedilo.split(' ') : '';
  var slike = '';
  for (var idx = 0; idx < besede.length; idx++) {
    var beseda = besede[idx].replace(/[\.,?!;](\s+)?$/, '');
    if (beseda.indexOf('http://') === 0 && 
      (beseda.endsWith(".gif") || beseda.endsWith(".jpg") || 
        beseda.endsWith(".png")))
    {
     slike += " <img style='width: 200px; padding-left:20px;' src='" + beseda + "' /><br />";
    }
  }
  
  return slike;
}

function izlusciInPripniSlike(vhodnoBesedilo, element)
{
  var slike = izlusciSlike(vhodnoBesedilo);
  if (slike.length > 0)
  {
    element.append(slike);
  }
}
/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var poke = sporocilo.indexOf('<html>&#9756;</html>');

  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  }
  else if (poke) {
    return divElementHtmlTekst(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
    izlusciInPripniSlike(sistemskoSporocilo, $('#sporocila'));
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    slike = izlusciSlike(sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    izlusciInPripniSlike(sporocilo, $('#sporocila'));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

function pridobiImeZaPrikazUporabnika(uporabnik) {
  return nadimkiUporabnikov[uporabnik] ? 
    (nadimkiUporabnikov[uporabnik] + " (" + uporabnik + ")") :
    uporabnik;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var nadimkiUporabnikov = {
  pridobiUporabnikaPoNadimku: function(nadimek){
    for (var key in this) {
      if (this[key] === nadimek) return key; 
    }
    return null;
  }
};

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    if (sporocilo.uporabnik)
    {
      sporocilo.besedilo = 
          sporocilo.besedilo.replace(
            sporocilo.uporabnik,
            pridobiImeZaPrikazUporabnika(sporocilo.uporabnik));
    }
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    izlusciInPripniSlike(sporocilo.besedilo, $('#sporocila'));
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(
        divElementEnostavniTekst(pridobiImeZaPrikazUporabnika(uporabniki[i])));
    }
    $('#seznam-uporabnikov div').click(function() {
    var prejemnik = $(this).text().split(' ');
    var imeUporabnika = nadimkiUporabnikov.pridobiUporabnikaPoNadimku(prejemnik[0]);
    if(!imeUporabnika) {
      imeUporabnika = prejemnik[0];
    }
    var izpis = klepetApp.procesirajUkaz('/zasebno ' +'"' + imeUporabnika + '" ' + '"' + '<html>&#9756;</html>' +'"');
    $('#sporocila').append(divElementHtmlTekst(izpis));
    $('#poslji-sporocilo').focus();
     });
  });
  
  
  socket.on('preimenovanUporabnik', function(uporabnik) {
    if (nadimkiUporabnikov[uporabnik.staroIme])
    {
      nadimkiUporabnikov[uporabnik.novoIme] = nadimkiUporabnikov[uporabnik.staroIme];
      delete nadimkiUporabnikov[uporabnik.staroIme];
    }
  });  

  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});